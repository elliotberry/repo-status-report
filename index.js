const path = require("path");
const chalk = require('chalk');
function execShellCommand(cmd) {
  const exec = require("child_process").exec;
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        console.warn(error);
      }
      resolve(stdout ? stdout : stderr);
    });
  });
}

async function main() {
    console.log(chalk.grey("Running find"));

  let g = await execShellCommand(
    'fd ".git" "/Users/eberry/projects/" -H -I -t d -a'
  );
  console.log(chalk.grey("Find finished."));
  let j = g.split("\n");

  let t = j.filter(function (item) {
    let ret = true;
    let o = path.parse(item);
    if (o.name !== ".git") {
      ret = false;
    }
    if (item.includes("node_modules")) {
      ret = false;
    }
    return ret;
  });
  let arr1 = [];
  let arr2 = [];
  for await (dir of t) {
      let j = path.parse(dir);
      let thedir = j.dir;
      let i = await execShellCommand(`cd "${thedir}" && git status`);
      if (i.includes("nothing to commit")) {
          arr1.push(thedir);
          //console.log(`${thedir}: Everything updated!`);
      }
      else {
        arr2.push(thedir);
        //console.log(`${thedir}:There are local changes.`);
      }
  }
  
  console.log(chalk.bold("Changed locally"));
  console.log(chalk.bgRed.white(arr2.join("\n")));

  console.log(chalk.bold("All synched lol: "));
  console.log(chalk.bgGreen.white(arr1.join("\n")));
}
main();
